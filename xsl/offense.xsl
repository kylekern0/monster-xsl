<!--
Copyright (c) 2020, Kyle Kern <kylekern0@protonmail.com>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-->

<!-- offense.xsl: Layout for the offense section -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	
	<!-- Includes: -->
	<xsl:import href="./list.xsl"/>
	<xsl:import href="./common.xsl"/>
	<xsl:import href="./dnd5e.xsl"/>
	
	<xsl:output method="html" omit-xml-declaration="yes"/>
	
	<!-- Offense - Combat moves: -->
	<xsl:template name="Monster-Offense">
		<xsl:element name="div">
			<xsl:attribute name="id">
				<xsl:value-of select="@name"/>
				<xsl:text> Offense</xsl:text>
			</xsl:attribute>
			
			<xsl:call-template name="section-heading">
				<xsl:with-param name="text" select="'Offense'"/>
			</xsl:call-template>
			
			<!-- Actions: -->
			<xsl:call-template name="Monster-Offense-MovesByTime">
				<xsl:with-param name="moveset" select="Moves[last()]"/>
				<xsl:with-param name="timename" select="'action'"/>
				<xsl:with-param name="title" select="'Actions'"/>
			</xsl:call-template>
			
			<!-- Bonus Actions: -->
			<xsl:call-template name="Monster-Offense-MovesByTime">
				<xsl:with-param name="moveset" select="Moves[last()]"/>
				<xsl:with-param name="timename" select="'bonus'"/>
				<xsl:with-param name="title" select="'Bonus Actions'"/>
			</xsl:call-template>
			
			<!-- Reactions: -->
			<xsl:call-template name="Monster-Offense-MovesByTime">
				<xsl:with-param name="moveset" select="Moves[last()]"/>
				<xsl:with-param name="timename" select="'reaction'"/>
				<xsl:with-param name="title" select="'Reactions'"/>
			</xsl:call-template>
			
			<!-- Cast Time: -->
			<xsl:call-template name="Monster-Offense-MovesByTime">
				<xsl:with-param name="moveset" select="Moves[last()]"/>
				<xsl:with-param name="timename" select="'casttime'"/>
				<xsl:with-param name="title" select="'Cast Time'"/>
			</xsl:call-template>
			
			<!-- Legendary Actions: -->
			<xsl:call-template name="Monster-Offense-MovesByTime">
				<xsl:with-param name="moveset" select="Moves[last()]"/>
				<xsl:with-param name="timename" select="'legendary'"/>
				<xsl:with-param name="title" select="'Legendary Actions'"/>
			</xsl:call-template>
		</xsl:element>
	</xsl:template>
	
	<!-- Combat moves of a specified cast time: -->
	<xsl:template name="Monster-Offense-MovesByTime">
		<xsl:param name="moveset"/>   <!-- Reference to the container of moves to search -->
		<xsl:param name="timename"/>  <!-- Type of action to search for (action/bonus/reaction/other) -->
		<xsl:param name="title"/>     <!-- Printed title of the section -->
		
		<xsl:if test="count($moveset/Move[@time=$timename]) > 0">
			<xsl:element name="div">
				<xsl:attribute name="id">
					<xsl:value-of select="@name"/>
					<xsl:text> Offense </xsl:text>
					<xsl:value-of select="$title"/>
				</xsl:attribute>

				<xsl:call-template name="feature-heading-paragraph">
					<xsl:with-param name="text" select="$title"/>
				</xsl:call-template>

				<xsl:element name="div">
					<xsl:attribute name="class"><xsl:text>indented</xsl:text></xsl:attribute>

					<xsl:apply-templates select="$moveset/Move[@time=$timename]"/>
				</xsl:element>
			</xsl:element>
		</xsl:if>
	</xsl:template>
	
	<!-- A single combat move: -->
	<xsl:template match="Move">
		<!-- Get the attribute mod needed for this move: -->
		<xsl:variable name="this_base_attr" select="./@attribute"/>
		<!--<xsl:variable name="base_attr_score" select="number(((((ancestor::Monster)[1])/Attributes[last()])/Attribute[@name=$this_base_attr])[last()]"/>-->
		<xsl:variable name="base_attr_score" select="((../../Attributes[last()])/Attribute[@name=$this_base_attr])[last()]"/>  <!-- Depends on the move being located in Monster/Moves/ -->
		<xsl:variable name="base_attr_mod"><xsl:call-template name="mod-from-score"><xsl:with-param name="score" select="$base_attr_score"/></xsl:call-template></xsl:variable>
		
		<!-- Get the global proficiency bonus: -->
		<xsl:variable name="global_prof_bonus" select="number((ancestor::Monster)[1]/ProficiencyBonus[last()])"/>
		
		<!-- Ensure that the proficiency factor for this move, if any, is a number: -->
		<xsl:variable name="sanitized_proficiency_factor">
			<xsl:choose>
				<xsl:when test="not(@proficient) or not(number(@proficient) = @proficient)">
					<xsl:value-of select="number(0)"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="number(@proficient)"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		
		<xsl:variable name="this_targets" select="./@target"/>
		
		<xsl:element name="p">
			<xsl:attribute name="class"><xsl:text>padded</xsl:text></xsl:attribute>
			
			<!-- Name: -->
			<xsl:call-template name="feature-heading-inline">
				<xsl:with-param name="text">
					<xsl:value-of select="@name"/>
					<xsl:text>: </xsl:text>
				</xsl:with-param>
			</xsl:call-template>
			
			<!-- Type: -->
			<xsl:if test="@movetype">
				<xsl:value-of select="@movetype"/>
				<xsl:text>, </xsl:text>
			</xsl:if>
			
			<!-- Casting time, if applicable: -->
			<xsl:if test="@time = 'casttime'">
				<xsl:text>time </xsl:text>
				<xsl:value-of select="@casttime"/>
				<xsl:text>, </xsl:text>
			</xsl:if>
			
			<!-- Deal with the different choices for the to-hit (i_roll, you_save, none): -->
			<xsl:choose>
				<!-- The creature rolls to hit: -->
				<xsl:when test="@tohittype='i_roll'">
					<xsl:call-template name="plus-if-positive">
						<xsl:with-param name="value">
							<xsl:choose>
								<!-- A value is set explicitly: -->
								<xsl:when test="@tohit">
									<xsl:value-of select="@tohit"/>
								</xsl:when>
								<!-- None given, we need to calculate it: -->
								<xsl:otherwise>
									<xsl:value-of select="number($base_attr_mod + ($global_prof_bonus * $sanitized_proficiency_factor))"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
					</xsl:call-template>
					<xsl:text> to hit, </xsl:text>
				</xsl:when>
				<!-- The target makes a saving throw: -->
				<xsl:when test="@tohittype='you_save'">
					<xsl:text>DC </xsl:text>
					<xsl:choose>
						<!-- A value is set explicitly: -->
						<xsl:when test="@tohit">
							<xsl:value-of select="@tohit"/>
						</xsl:when>
						<!-- None given, we need to calculate it: -->
						<xsl:otherwise>
							<xsl:value-of select="number(8 + $base_attr_mod + $global_prof_bonus)"/>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:text> </xsl:text>
					<xsl:choose>
						<xsl:when test="./@save_attribute"><xsl:value-of select="./@save_attribute"/></xsl:when>
						<xsl:otherwise><xsl:value-of select="$this_base_attr"/></xsl:otherwise>
					</xsl:choose>
					<xsl:text> saving throw, </xsl:text>
				</xsl:when>
				<!-- Custom text: -->
				<xsl:otherwise>
					<xsl:if test="@tohit">
						<xsl:value-of select="@tohit"/>
						<xsl:text>, </xsl:text>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
			
			<!-- Targeting: -->
			<xsl:if test="./@reach">
				<xsl:text>reach </xsl:text>
				<xsl:value-of select="./@reach"/>
				<xsl:if test="./@unit">
					<xsl:text> </xsl:text>
					<xsl:value-of select="./@unit"/>
				</xsl:if>
				<xsl:text>, </xsl:text>
			</xsl:if>
			<xsl:if test="./@range">
				<xsl:text>range </xsl:text>
				<xsl:value-of select="./@range"/>
				<xsl:if test="./@unit">
					<xsl:text> </xsl:text>
					<xsl:value-of select="./@unit"/>
				</xsl:if>
				<xsl:text>, </xsl:text>
			</xsl:if>
			<xsl:if test="$this_targets">
				<xsl:value-of select="$this_targets"/>
				<xsl:text>. </xsl:text>
			</xsl:if>
			
			<!-- Damage: -->
			<xsl:if test="@tohittype">
				<xsl:text>On hit: </xsl:text>
			</xsl:if>
			<xsl:apply-templates select="DamageType"/>
			
			<!-- Description: -->
			<xsl:call-template name="description-inline">
				<xsl:with-param name="text" select="Description"/>
			</xsl:call-template>
		</xsl:element>
	</xsl:template>
	
	<!-- Damage die: -->
	<xsl:template match="DamageType">
		<xsl:variable name="die_to_use" select="Die[last()]"/>
		<xsl:variable name="this_count">
			<xsl:choose>
				<xsl:when test="not($die_to_use/@count) or not(number($die_to_use/@count) = $die_to_use/@count)">
					<xsl:value-of select="number(1)"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="number($die_to_use/@count)"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="this_sides" select="number($die_to_use/@sides)"/>
		<xsl:variable name="this_const">
			<xsl:choose>
				<xsl:when test="not($die_to_use/@const) or not(number($die_to_use/@const) = $die_to_use/@const)">
					<xsl:value-of select="number(0)"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="number($die_to_use/@const)"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		
		<xsl:call-template name="die-average">
			<xsl:with-param name="count" select="$this_count"/>
			<xsl:with-param name="sides" select="$this_sides"/>
			<xsl:with-param name="const" select="$this_const"/>
		</xsl:call-template>
		
		<xsl:text> (</xsl:text>
		<xsl:value-of select="$this_count"/>
		<xsl:text>d</xsl:text>
		<xsl:value-of select="$this_sides"/>
		<xsl:if test="$this_const > 0">
			<xsl:call-template name="plus-if-positive">
				<xsl:with-param name="value" select="$this_const"/>
			</xsl:call-template>
		</xsl:if>
		<xsl:text>) </xsl:text>
		
		<xsl:value-of select="./@name"/>
		
		<xsl:if test="string-length(Description) > 0">
			<xsl:text> </xsl:text>
			<xsl:call-template name="description-inline">
				<xsl:with-param name="text" select="Description"/>
			</xsl:call-template>
		</xsl:if>
		
		<xsl:choose>
			<xsl:when test="count(following-sibling::DamageType) > 0">
				<xsl:text> + </xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>. </xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
</xsl:stylesheet>
