<!--
Copyright (c) 2020, Kyle Kern <kylekern0@protonmail.com>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-->

<!-- scoresfeatures.xsl: Layout for the attributes, proficiencies, and features section -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	
	<!-- Includes: -->
	<xsl:import href="./list.xsl"/>
	<xsl:import href="./common.xsl"/>
	<xsl:import href="./dnd5e.xsl"/>
	
	<xsl:output method="html" omit-xml-declaration="yes"/>
	
	<!-- Scores and Features - Anything that isn't directly offense- or defense-related -->
	<xsl:template name="Monster-Features">
		
		<!-- Proficiency bonus as a number: -->
		<xsl:variable name="this_prof_bonus">
			<xsl:choose>
				<xsl:when test="number(ProficiencyBonus[last()]) = ProficiencyBonus[last()]">
					<xsl:value-of select="number(ProficiencyBonus[last()])"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="number(0)"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		
		<xsl:element name="div">
			<xsl:attribute name="id">
				<xsl:value-of select="@name"/>
				<xsl:text> Features</xsl:text>
			</xsl:attribute>
			
			<!-- Attribute Scores: -->
			<xsl:apply-templates select="Attributes[last()]"/>
			
			<!-- Movement speeds: -->
			<xsl:call-template name="list-of-values">
				<xsl:with-param name="container" select="Speeds[last()]"/>
				<xsl:with-param name="attributes" select="Attributes[last()]"/>
				<xsl:with-param name="profbonus" select="$this_prof_bonus"/>
				<xsl:with-param name="heading" select="'Speed'"/>
				<xsl:with-param name="values_mode" select="'unsigned'"/>
			</xsl:call-template>

			<!-- Senses: -->
			<xsl:call-template name="list-of-values">
				<xsl:with-param name="container" select="Senses[last()]"/>
				<xsl:with-param name="attributes" select="Attributes[last()]"/>
				<xsl:with-param name="profbonus" select="$this_prof_bonus"/>
				<xsl:with-param name="heading">Senses</xsl:with-param>
				<xsl:with-param name="values_mode" select="'unsigned'"/>
			</xsl:call-template>

			<!-- Skills: -->
			<xsl:call-template name="list-of-values">
				<xsl:with-param name="container" select="Skills[last()]"/>
				<xsl:with-param name="attributes" select="Attributes[last()]"/>
				<xsl:with-param name="profbonus" select="$this_prof_bonus"/>
				<xsl:with-param name="heading">Skills</xsl:with-param>
			</xsl:call-template>
			
			<!-- Languages: -->
			<xsl:call-template name="list-of-values">
				<xsl:with-param name="container" select="Languages[last()]"/>
				<xsl:with-param name="attributes" select="Attributes[last()]"/>
				<xsl:with-param name="heading" select="'Languages'"/>
				<xsl:with-param name="values_mode" select="'unsigned'"/>
			</xsl:call-template>

			<!-- Features: -->
			<xsl:apply-templates select="Features/Feature"/>
		</xsl:element>
		
	</xsl:template>
	
	<!-- Attribute Scores: -->
	<xsl:template match="Attributes[last()]">
		<xsl:element name="table">
			<xsl:attribute name="class"><xsl:text>attr-table</xsl:text></xsl:attribute>
			
			<xsl:apply-templates select="Attribute"/>
		</xsl:element>
	</xsl:template>
	
	<!-- Attribute Score: -->
	<xsl:template match="Attribute">
		<xsl:variable name="modifier">
			<xsl:call-template name="mod-from-score">
				<xsl:with-param name="score" select="."/>
			</xsl:call-template>
		</xsl:variable>
		
		<xsl:element name="tr">
			<xsl:element name="th"><xsl:call-template name="abbreviate"><xsl:with-param name="full" select="@name"/></xsl:call-template></xsl:element>
			<xsl:element name="th"><xsl:call-template name="plus-if-positive"><xsl:with-param name="value" select="$modifier"/></xsl:call-template></xsl:element>
			<xsl:element name="td"><xsl:call-template name="parenthetical"><xsl:with-param name="text" select="."/></xsl:call-template></xsl:element>
		</xsl:element>
	</xsl:template>
	
	<!-- Feature: -->
	<xsl:template match="Feature">
		<xsl:element name="p">
			<xsl:attribute name="class"><xsl:text>padded</xsl:text></xsl:attribute>
			
			<xsl:element name="span">
				<xsl:attribute name="class"><xsl:text>feature-heading</xsl:text></xsl:attribute>
				
				<xsl:value-of select="@name"/>
				<xsl:text>: </xsl:text>
			</xsl:element>
			
			<xsl:value-of select="."/>
		</xsl:element>
	</xsl:template>
	
</xsl:stylesheet>
