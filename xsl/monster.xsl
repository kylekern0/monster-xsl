<!--
Copyright (c) 2020, Kyle Kern <kylekern0@protonmail.com>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-->

<!-- monster.xsl: Layout for a single monster's stat block -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	
	<!-- Includes: -->
	<xsl:import href="./scoresfeatures.xsl"/>
	<xsl:import href="./defense.xsl"/>
	<xsl:import href="./offense.xsl"/>
	
	<xsl:output method="html" omit-xml-declaration="yes"/>
	
	<!-- Monster - A single stat block: -->
	<xsl:template match="Monster">
		
		<xsl:element name="div">
			<xsl:attribute name="id">
				<xsl:value-of select="@name"/>
			</xsl:attribute>
			<xsl:attribute name="class">
				<xsl:text>Statblock</xsl:text>
				<xsl:value-of select="position()"/>
			</xsl:attribute>
			
			<!-- Theme: -->
			<xsl:call-template name="Monster-Theme"/>
			
			<!-- Title: -->
			<xsl:call-template name="Monster-Title"/>
			
			<!-- Scores and Features: -->
			<xsl:call-template name="Monster-Features"/>
			
			<!-- Defense: -->
			<xsl:call-template name="Monster-Defense"/>
			
			<!-- Offense: -->
			<xsl:call-template name="Monster-Offense"/>
			
			<!-- Description: -->
			<xsl:call-template name="Monster-Description"/>
		</xsl:element>
		
	</xsl:template>
	
	<!-- Theme - A customized color scheme for this stat block -->
	<xsl:template name="Monster-Theme">
		<!-- Only bother if the author actually specified a theme: -->
		<xsl:if test="Theme">
			<!-- Locally-valid copy of the last theme in the context Monster node: -->
			<xsl:variable name="this_theme" select="Theme[last()]"/>
			
			<xsl:element name="style">
				<xsl:text>div.Statblock</xsl:text>
				<xsl:value-of select="position()"/>
				<xsl:text> {</xsl:text>
				<!-- Background Color: -->
				<xsl:text>--backgnd-color:#</xsl:text>
				<xsl:choose>
					<xsl:when test="$this_theme/@background"><xsl:value-of select="$this_theme/@background"/></xsl:when>
					<xsl:otherwise><xsl:value-of select="'ffffff'"/></xsl:otherwise>
				</xsl:choose>
				<xsl:text>; </xsl:text>
				<!-- Normal Color: -->
				<xsl:text>--normal-color:#</xsl:text>
				<xsl:choose>
					<xsl:when test="$this_theme/@normal"><xsl:value-of select="$this_theme/@normal"/></xsl:when>
					<xsl:otherwise><xsl:value-of select="'000000'"/></xsl:otherwise>
				</xsl:choose>
				<xsl:text>; </xsl:text>
				<!-- Intense Color: -->
				<xsl:text>--intense-color:#</xsl:text>
				<xsl:choose>
					<xsl:when test="$this_theme/@intense"><xsl:value-of select="$this_theme/@intense"/></xsl:when>
					<xsl:otherwise><xsl:value-of select="'000000'"/></xsl:otherwise>
				</xsl:choose>
				<xsl:text>; </xsl:text>
				
				<!-- We need to re-specify the background color for some reason: -->
				<xsl:text>background-color:var(--backgnd-color); </xsl:text>
			</xsl:element>
		</xsl:if>
	</xsl:template>
	
	<!-- Title - Monster name, types, and author info -->
	<xsl:template name="Monster-Title">
		<xsl:element name="div">
			<xsl:attribute name="id">
				<xsl:value-of select="@name"/>
				<xsl:text> Title</xsl:text>
			</xsl:attribute>
			
			<!-- Name: -->
			<xsl:element name="h1">
				<xsl:attribute name="style">
					<xsl:text>margin-bottom:10px;</xsl:text>
				</xsl:attribute>
				
				<xsl:value-of select="@name"/>
			</xsl:element>
			
			<!-- Type: -->
			<xsl:element name="p">
				<xsl:attribute name="style">
					<xsl:text>margin-bottom:10px;</xsl:text>
				</xsl:attribute>
				
				<!--<xsl:for-each select="@size | @alignment | @type">
					<xsl:element name="b">
						<xsl:value-of select="."/>
						<xsl:text> </xsl:text>
					</xsl:element>
				</xsl:for-each>-->
				<xsl:element name="b">
					<xsl:value-of select="@size"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="@type"/>
					<xsl:text>, </xsl:text>
					<xsl:value-of select="@alignment"/>
				</xsl:element>
			</xsl:element>
			
			<!-- Author: -->
			<xsl:apply-templates select="Copyright"/>
		</xsl:element>
	</xsl:template>
	
	<!-- Copyright - Author and usage rights information -->
	<xsl:template match="Copyright">
		<!-- Only display this section if either a name or email address are given: -->
		<xsl:if test="Author | Email">
			<xsl:element name="p">
				<xsl:text>Copyright </xsl:text>
				
				<!-- The copyright date, if provided: -->
				<xsl:if test="Date">
					<xsl:value-of select="Date[last()]"/>
					<xsl:text> </xsl:text>
				</xsl:if>
				
				<!-- The author name, if provided: -->
				<xsl:if test="Author">
					<xsl:value-of select="Author[last()]"/>
					<xsl:if test="Email">
						<xsl:text> </xsl:text>
					</xsl:if>
				</xsl:if>
				
				<!-- The author's email address (with a link), if provided: -->
				<xsl:if test="Email">
					<xsl:variable name="email_address" select="Email[last()]"/>
					<xsl:text>&lt;</xsl:text>
					<xsl:element name="a">
						<xsl:attribute name="href">
							<xsl:text>mailto:</xsl:text>
							<xsl:value-of select="$email_address"/>
						</xsl:attribute>
						<xsl:value-of select="$email_address"/>
					</xsl:element>
					<xsl:text>&gt;</xsl:text>
				</xsl:if>
				
				<xsl:text>.</xsl:text>
				
				<!-- Custom license text, if provided: -->
				<xsl:if test="License">
					<xsl:text> </xsl:text>
					<xsl:value-of select="License[last()]"/>
				</xsl:if>
			</xsl:element>
		</xsl:if>
	</xsl:template>
	
	<!-- Monster description: -->
	<xsl:template name="Monster-Description">
		<xsl:if test="Description | Image">
			<xsl:element name="div">
				<xsl:attribute name="id">
					<xsl:value-of select="@name"/>
					<xsl:text> Description</xsl:text>
				</xsl:attribute>
				
				<xsl:call-template name="section-heading">
					<xsl:with-param name="text" select="'Description'"/>
				</xsl:call-template>
				
				<!-- Description text: -->
				<xsl:call-template name="description-paragraph">
					<xsl:with-param name="text" select="Description[last()]"/>
				</xsl:call-template>
				
				<xsl:if test="Image">
					<xsl:element name="p">
						<xsl:attribute name="class"><xsl:text>padded</xsl:text></xsl:attribute>
						<xsl:attribute name="align"><xsl:text>center</xsl:text></xsl:attribute>
						
						<xsl:if test="Image/@credit">
							<xsl:text>Art by </xsl:text>
							<xsl:value-of select="Image/@credit[last()]"/>
							<xsl:element name="br"/>
						</xsl:if>
						<xsl:element name="img">
							<xsl:attribute name="src"><xsl:value-of select="Image"/></xsl:attribute>
							<xsl:attribute name="width"><xsl:text>90%</xsl:text></xsl:attribute>
							
							<xsl:if test="(number(Image/@max-width) = Image/@max-width) or (number(Image/@max-height) = Image/@max-height)">
								<xsl:attribute name="style">
									<xsl:if test="number(Image/@max-width) = Image/@max-width">
										<xsl:text>max-width:</xsl:text>
										<xsl:value-of select="Image/@max-width"/>
										<xsl:text>px;</xsl:text>
									</xsl:if>
									<xsl:if test="number(Image/@max-height) = Image/@max-height">
										<xsl:text>max-height:</xsl:text>
										<xsl:value-of select="Image/@max-height"/>
										<xsl:text>px;</xsl:text>
									</xsl:if>
								</xsl:attribute>
							</xsl:if>
						</xsl:element>
					</xsl:element>
			</xsl:if>
			</xsl:element>
		</xsl:if>
	</xsl:template>

</xsl:stylesheet>
