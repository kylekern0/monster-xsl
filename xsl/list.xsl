<!--
Copyright (c) 2020, Kyle Kern <kylekern0@protonmail.com>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-->

<!-- list.xsl: Single-line lists of terms, potentially with accompanying values -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	
	<!-- Includes: -->
	<xsl:import href="./common.xsl"/>
	<xsl:import href="./dnd5e.xsl"/>
	
	<xsl:output method="html" omit-xml-declaration="yes"/>
	
	<!-- Create a named list of values: -->
	<xsl:template name="list-of-values">
		<xsl:param name="container"/>    <!-- Reference to parent node of the list items -->
		<xsl:param name="attributes"/>   <!-- Reference to the attributes table to use -->
		<xsl:param name="profbonus"/>    <!-- Proficiency bonus -->
		<xsl:param name="heading"/>      <!-- Printed name of the list -->
		<xsl:param name="abbreviate"/>   <!-- Set to "true" to abbreviate list item names -->
		<xsl:param name="values_mode"/>  <!-- Modifies the way that list item values are printed:
		                                        "no-values" = Just print the names
		                                        "unsigned" = Don't print + sign for positive values -->
		
		<xsl:element name="p">
			<xsl:element name="span">
				<xsl:attribute name="class"><xsl:text>feature-heading</xsl:text></xsl:attribute>
				
				<xsl:value-of select="$heading"/>
				<xsl:text>: </xsl:text>
			</xsl:element>
			
			<!-- If the list doesn't contain anything, print this instead: -->
			<xsl:if test="count($container/*) = 0">
				<xsl:text>-</xsl:text>
			</xsl:if>
		
			<xsl:for-each select="$container/*">
				<xsl:call-template name="list-of-values-item">
					<xsl:with-param name="item" select="."/>
					<xsl:with-param name="attributes" select="$attributes"/>
					<xsl:with-param name="profbonus" select="$profbonus"/>
					<xsl:with-param name="abbreviate" select="$abbreviate"/>
					<xsl:with-param name="value_mode" select="$values_mode"/>
				</xsl:call-template>
				
				<xsl:if test="count(preceding-sibling::*) != count(../*) - 1">
					<xsl:text>, </xsl:text>
				</xsl:if>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>
	
	<!-- Create a single item from a list of values: -->
	<xsl:template name="list-of-values-item">
		<xsl:param name="item"/>        <!-- Reference to the list item node -->
		<xsl:param name="attributes"/>  <!-- Reference to the attributes table to use -->
		<xsl:param name="profbonus"/>   <!-- Proficiency bonus -->
		<xsl:param name="abbreviate"/>  <!-- Set to "true" to abbreviate item name -->
		<xsl:param name="value_mode"/>  <!-- Modifies the way that list item value is printed:
		                                       "no-values" = Just print the names
		                                       "unsigned" = Don't print + sign for positive values -->
		
		<!-- If the element doesn't have a name, use the name of the attribute instead: -->
		<xsl:variable name="printed_name">
			<xsl:choose>
				<xsl:when test="$item/@name"><xsl:value-of select="$item/@name"/></xsl:when>
				<xsl:otherwise><xsl:value-of select="$item/@attribute"/></xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		
		<!-- Abbreviate the item name if enabled: -->
		<xsl:choose>
			<xsl:when test="$abbreviate = 'true'">
				<xsl:call-template name="abbreviate">
					<xsl:with-param name="full" select="$printed_name"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$printed_name"/>
			</xsl:otherwise>
		</xsl:choose>
		
		<!-- Only print the value if the "no-values" mode wasn't set: -->
		<!-- Also, skip the value for a single item if one isn't specified - this allows mixed valued and valueless lists -->
		<xsl:if test="($value_mode != 'no-values') and ((string-length($item) > 0) or ($item/@attribute) or ($item/@proficient))">
			<xsl:text> </xsl:text>
		
			<!-- The value: -->
			<xsl:variable name="unsigned_value">
				<xsl:choose>
					<!-- Prioritize the manually specified value, if it exists: -->
					<xsl:when test="$item != ''"><xsl:value-of select="$item"/></xsl:when>

					<!-- Otherwise, we'll need to calculate the mod based on the underlying attribute score: -->
					<xsl:otherwise>
						<xsl:variable name="base_attr_mod">
							<xsl:call-template name="mod-from-score">
								<xsl:with-param name="score" select="$attributes/Attribute[@name=($item/@attribute)]"/>
							</xsl:call-template>
						</xsl:variable>

						<xsl:choose>
							<!-- If there is no proficiency bonus, just use the base attribute's mod: -->
							<xsl:when test="not($item/@proficient)"><xsl:value-of select="$base_attr_mod"/></xsl:when>

							<!-- Otherwise, add the proficiency bonus to the base attribute mod: -->
							<xsl:otherwise><xsl:value-of select="$base_attr_mod + ($profbonus * @proficient)"/></xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			
			<!-- Display the value with its sign, if opted: -->
			<xsl:choose>
				<xsl:when test="$value_mode = 'unsigned'">
					<xsl:value-of select="$unsigned_value"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="plus-if-positive">
						<xsl:with-param name="value" select="$unsigned_value"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
			
			<!-- The unit, if supplied: -->
			<xsl:if test="$item/@unit">
				<xsl:text> </xsl:text>
				<xsl:value-of select="$item/@unit"/>
			</xsl:if>
		</xsl:if>
	</xsl:template>
	
</xsl:stylesheet>
