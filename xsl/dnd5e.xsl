<!--
Copyright (c) 2020, Kyle Kern <kylekern0@protonmail.com>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-->

<!-- dnd5e.xsl: Dungeons and Dragons 5th Edition calculations -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	
	<xsl:output method="html" omit-xml-declaration="yes"/>
	
	<!-- Calculate a modifier from a score: -->
	<xsl:template name="mod-from-score">
		<xsl:param name="score"/>
		<xsl:value-of select="floor(($score - 10) div 2)"/>
	</xsl:template>
	
	<!-- Average roll for a set of dice: -->
	<xsl:template name="sum-die-averages">
		<xsl:param name="this_die"/>  <!-- Required: Reference to the next die to add - non-recursive (initial) calls should specify the first die in a container -->
		<xsl:param name="prev_sum"/>  <!-- Required: Sum after adding the previous die - non-recursive (initial) calls should specify 0 -->
		<xsl:param name="const"/>     <!-- Optional: Constant to add to the total average (default 0) -->
		
		<!-- Get the average from the current die: -->
		<xsl:variable name="this_avg">
			<xsl:call-template name="die-average">
				<xsl:with-param name="sides" select="$this_die/@sides"/>
				<xsl:with-param name="count" select="$this_die/@count"/>
				<xsl:with-param name="const" select="$this_die/@const"/>
			</xsl:call-template>
		</xsl:variable>
		
		<!-- Add this average to the previous total: -->
		<xsl:variable name="this_sum" select="number($prev_sum + $this_avg)"/>
		
		<xsl:choose>
			<!-- If there is another die in this container, recurse into this template to handle it: -->
			<xsl:when test="count($this_die/following-sibling::Die) > 0">
				<xsl:call-template name="sum-die-averages">
					<xsl:with-param name="this_die" select="($this_die/following-sibling::Die)[1]"/>
					<xsl:with-param name="prev_sum" select="$this_sum"/>
				</xsl:call-template>
			</xsl:when>
			
			<!-- Otherwise, we've finished - add the constant, if any, and return the total: -->
			<xsl:otherwise>
				<xsl:value-of select="number($this_sum + $const)"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<!-- Average roll for a single die: -->
	<xsl:template name="die-average">
		<xsl:param name="sides"/>  <!-- Required: Number of sides on this type of die -->
		<xsl:param name="count"/>  <!-- Optional: Number of dice of this type (default 1) -->
		<xsl:param name="const"/>  <!-- Optional: Constant modifier (default 0) -->
		
		<!-- Handle missing optional parameters by selecting defaults: -->
		<xsl:variable name="this_sides"><xsl:choose>
			<xsl:when test="number($sides) = $sides"><xsl:value-of select="number($sides)"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="number(0)"/></xsl:otherwise>
		</xsl:choose></xsl:variable>
		<xsl:variable name="this_count"><xsl:choose>
			<xsl:when test="number($count) = $count"><xsl:value-of select="number($count)"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="number(1)"/></xsl:otherwise>
		</xsl:choose></xsl:variable>
		<xsl:variable name="this_const"><xsl:choose>
			<xsl:when test="number($const) = $const"><xsl:value-of select="number($const)"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="number(0)"/></xsl:otherwise>
		</xsl:choose></xsl:variable>
		
		<xsl:value-of select="number(floor((($this_sides + 1) div 2) * $this_count) + $this_const)"/>
	</xsl:template>
	
</xsl:stylesheet>
