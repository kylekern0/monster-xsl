<!--
Copyright (c) 2020, Kyle Kern <kylekern0@protonmail.com>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-->

<!-- common.xsl: "Building block" templates for small formatting tasks -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	
	<xsl:output method="html" omit-xml-declaration="yes"/>
	
	<!-- Section Heading: -->
	<xsl:template name="section-heading">
		<xsl:param name="text"/>
		
		<xsl:element name="p">
			<xsl:attribute name="class"><xsl:text>section-heading</xsl:text></xsl:attribute>
			
			<xsl:value-of select="$text"/>
		</xsl:element>
	</xsl:template>
	
	<!-- Feature heading as a separate paragraph: -->
	<xsl:template name="feature-heading-paragraph">
		<xsl:param name="text"/>
		
		<xsl:element name="p">
			<xsl:attribute name="class"><xsl:text>padded</xsl:text></xsl:attribute>
			
			<xsl:call-template name="feature-heading-inline">
				<xsl:with-param name="text" select="$text"/>
			</xsl:call-template>
		</xsl:element>
	</xsl:template>
	
	<!-- Feature heading inline with the feature: -->
	<xsl:template name="feature-heading-inline">
		<xsl:param name="text"/>
		
		<xsl:element name="span">
			<xsl:attribute name="class"><xsl:text>feature-heading</xsl:text></xsl:attribute>
			
			<xsl:value-of select="$text"/>
		</xsl:element>
	</xsl:template>
	
	<!-- Description as a paragraph: -->
	<xsl:template name="description-paragraph">
		<xsl:param name="text"/>   <!-- Required: description text -->
		<xsl:param name="class"/>  <!-- Optional: formatting class to apply to the paragraph -->
		
		<xsl:element name="p">
			<xsl:if test="$class">
				<xsl:attribute name="class">
					<xsl:value-of select="$class"/>
				</xsl:attribute>
			</xsl:if>
			
			<xsl:call-template name="description-inline">
				<xsl:with-param name="text" select="$text"/>
			</xsl:call-template>
		</xsl:element>
	</xsl:template>
	
	<!-- Description inline with other text: -->
	<xsl:template name="description-inline">
		<xsl:param name="text"/>
		
		<xsl:value-of select="$text"/>
	</xsl:template>
	
	<!-- Abbreviate attribute names to three capital letters: -->
	<xsl:template name="abbreviate">
		<xsl:param name="full"/>
		<span class="abbreviation"><xsl:value-of select="substring($full,1,3)"/></span>
	</xsl:template>
	
	<!-- Show a + sign in front of a positive number: -->
	<xsl:template name="plus-if-positive">
		<xsl:param name="value"/>
		
		<xsl:if test="$value > 0">
			<xsl:text>+</xsl:text>
		</xsl:if>
		<xsl:value-of select="$value"/>
	</xsl:template>
	
	<!-- Enclose text within parentheses: -->
	<xsl:template name="parenthetical">
		<xsl:param name="text"/>
		
		<xsl:text>(</xsl:text>
		<xsl:value-of select="$text"/>
		<xsl:text>)</xsl:text>
	</xsl:template>
	
	<!-- Print a set of dice: -->
	<xsl:template name="print-dice">
		<xsl:param name="dice"/>   <!-- Reference to a container of Die elements -->
		<xsl:param name="delim"/>  <!-- Text to place between dice -->
		
		<xsl:for-each select="$dice/Die">
			<xsl:call-template name="print-die">
				<xsl:with-param name="die" select="."/>
			</xsl:call-template>
			
			<xsl:if test="count(./following-sibling::Die) > 0">
				<xsl:value-of select="$delim"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	
	<!-- Print a die: -->
	<xsl:template name="print-die">
		<xsl:param name="die"/>  <!-- Reference to the Die to print -->
		
		<xsl:variable name="this_count"><xsl:choose>
			<xsl:when test="number($die/@count) = $die/@count"><xsl:value-of select="number($die/@count)"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="number(1)"/></xsl:otherwise>
		</xsl:choose></xsl:variable>
		<xsl:variable name="this_sides"><xsl:choose>
			<xsl:when test="number($die/@sides) = $die/@sides"><xsl:value-of select="number($die/@sides)"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="number(0)"/></xsl:otherwise>
		</xsl:choose></xsl:variable>
		<xsl:variable name="this_const"><xsl:choose>
			<xsl:when test="number($die/@const) = $die/@const"><xsl:value-of select="number($die/@const)"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="number(0)"/></xsl:otherwise>
		</xsl:choose></xsl:variable>
		
		<xsl:value-of select="$this_count"/>
		<xsl:text>d</xsl:text>
		<xsl:value-of select="$this_sides"/>
		<xsl:if test="not($this_const = 0)">
			<xsl:call-template name="plus-if-positive">
				<xsl:with-param name="value" select="$this_const"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	
	<!-- Counts dice within a container: -->
	<xsl:template name="count-dice">
		<xsl:param name="this_type"/>   <!-- Reference to a Die element at which to start counting -->
		<xsl:param name="prev_count"/>  <!-- Previous count of dice (0 for initial call) -->
		
		<!-- Number of dice specified by this element: -->
		<xsl:variable name="this_contrib">
			<xsl:choose>
				<xsl:when test="number($this_type/@count) = $this_type/@count">
					<xsl:value-of select="number($this_type/@count)"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="number(1)"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		
		<!-- Current count, up to this point: -->
		<xsl:variable name="this_count" select="number($prev_count + $this_contrib)"/>
		
		<xsl:choose>
			<!-- If there are dice remaining, recurse into them: -->
			<xsl:when test="count($this_type/following-sibling::Die) > 0">
				<xsl:call-template name="count-dice">
					<xsl:with-param name="this_type" select="($this_type/following-sibling::Die)[1]"/>
					<xsl:with-param name="prev_count" select="$this_count"/>
				</xsl:call-template>
			</xsl:when>
			
			<!-- Otherwise, we're done counting: -->
			<xsl:otherwise>
				<xsl:value-of select="$this_count"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
</xsl:stylesheet>
