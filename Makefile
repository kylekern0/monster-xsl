# GNU Make rules to generate the output HTML documents

# Directory containing the XSL code:
XSLDIR="./xsl"

# Get a list of the HTML files to generate (based on existing XML files):
XMLFILES="$(wildcard ./*.xml)
HTMFILES="$(patsubst ./*.xml, ./*.html, $(XMLFILES))

# By default, build all that need building:
all: $(HTMFILES)

# Rule to make HTML file from XML file:
%.html: %.xml
	xsltproc -o $@ "$(XSLDIR)/monstersheet.xsl" $<

