<!--
Copyright (c) 2020, Kyle Kern <kylekern0@protonmail.com>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-->

<!-- monstersheet.xsl: HTML document root templates -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	
	<!-- Includes: -->
	<xsl:import href="./monster.xsl"/>
	
	<xsl:output method="html" omit-xml-declaration="yes"/>
	
	<!-- HTML Document Root -->
	<xsl:template match="/MonsterSheet">
		<xsl:element name="HTML">
			<!-- Document Head -->
			<xsl:call-template name="document-head"/>
			
			<!-- Document Body - Create a statblock for each monster -->
			<xsl:element name="BODY">
				<!-- Table of Contents, if there is more than one monster: -->
				<xsl:call-template name="table-of-contents"/>
				
				<!-- The actual content (one statblock per monster): -->
				<xsl:apply-templates select="Monster"/>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<!-- Document Head - Title, Metadata and Stylesheets -->
	<xsl:template name="document-head">
		<xsl:element name="HEAD">
			<!-- Document title - Select the first three monsters to list -->
			<xsl:element name="title">
				<xsl:variable name="total_monster_count" select="count(Monster)"/>
				<xsl:variable name="first_three_monsters" select="(Monster)[position() &lt; 4]"/>
				<xsl:variable name="count_being_listed" select="count($first_three_monsters)"/>
				
				<xsl:for-each select="$first_three_monsters">
					<xsl:value-of select="@name"/>
					<xsl:choose>
						<!-- If there are more monsters described than we are listing in the title,
						     write "and more" at the end of the list: -->
						<xsl:when test="position() = $count_being_listed">
							<xsl:if test="$count_being_listed &lt; $total_monster_count">
								<xsl:text> and more</xsl:text>
							</xsl:if>
						</xsl:when>
						<!-- Handle commas and spacing between list items: -->
						<xsl:otherwise>
							<xsl:choose>
								<xsl:when test="position() = ($count_being_listed - 1)">
									<xsl:choose>
										<xsl:when test="$count_being_listed = $total_monster_count">
											<xsl:text> and</xsl:text>
										</xsl:when>
										<xsl:otherwise>
											<xsl:text>,</xsl:text>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>,</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text> </xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</xsl:element>
			
			<!-- Link to a CSS stylesheet to provide default formatting: -->
			<xsl:call-template name="css-style"/>
		</xsl:element>
	</xsl:template>
	
	<!-- Table of Contents - A link to each monster in a multi-statblock document: -->
	<xsl:template name="table-of-contents">
		<xsl:if test="count(Monster) > 1">
			<xsl:element name="div">
				<xsl:attribute name="id"><xsl:text>ToC</xsl:text></xsl:attribute>

				<xsl:element name="h4">
					<xsl:text>Table of Contents</xsl:text>
				</xsl:element>

				<xsl:element name="ul">
					<xsl:for-each select="Monster">
						<xsl:element name="li">
							<xsl:element name="a">
								<xsl:attribute name="href">
									<xsl:text>#</xsl:text>
									<xsl:value-of select="@name"/>
								</xsl:attribute>
								<xsl:value-of select="@name"/>
							</xsl:element>
						</xsl:element>
					</xsl:for-each>
				</xsl:element>
			</xsl:element>
		</xsl:if>
	</xsl:template>
	
	<!-- Embed a CSS stylesheet to provide default formatting: -->
	<xsl:template name="css-style">
		<xsl:element name="style">
			<xsl:attribute name="type"><xsl:text>text/css</xsl:text></xsl:attribute>
			<xsl:text>
					/* Document colors: */
					body {
						--intense-color: #000000;
						--backgnd-color: #ffffff;
						--normal-color: #000000;
						
						background-color: var(--backgnd-color);
					}

					.subsection-heading {
						margin-top: 10px;
						color: var(--intense-color);
					}

					div.indented {
						padding-left: 30px;
					}

					/* Remove whitespaces after paragraphs unless otherwise specified: */
					p {
						margin: 0;
						padding: 0;
						
						/* Also specify the default text color: */
						color: var(--normal-color);
					}

					/* ...Except for when we want there to be some space (features, attacks, etc). */
					p.padded {
						margin-top: 10px;
						padding-left: 30px;
						text-indent: -30px;
					}

					/* ... Except for headings, which provide some space between sections: */
					p.section-heading {
						margin-top: 30px;
						margin-bottom: 10px;
						
						/* Also give them some nifty formatting: */
						color: var(--intense-color);
						font-variant: small-caps;
						text-decoration: underline overline;
					}

					/* Give tables extra padding: */
					table {
						margin-top: 10px;
						margin-bottom: 10px;
					}

					/* Instead of capitalizing first letters in the title, use larger first letters: */
					h1 {
						font-variant: small-caps;
						color: var(--intense-color);
					}

					/* Capitalize all abbreviated text: */
					.abbreviation {
						text-transform: uppercase;
					}

					/* Turn table rows into columns and columns into rows for the attribute scores: */
					table.attr-table tr {
						display: table-cell;
					}
					table.attr-table tr td, table.attr-table tr th {
						display: block;
					}

					/* Improve table readability: */
					th, td {
						color: var(--intense-color);
						text-align: center;
						padding: 3px;
						padding-left: 12px;
						padding-right: 12px;
					}

					/* Apply the intense color to text headings and links: */
					.feature-heading, b, a {
						color: var(--intense-color);
					}
			</xsl:text>
		</xsl:element>
	</xsl:template>
	
</xsl:stylesheet>
