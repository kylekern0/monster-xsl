<!--
Copyright (c) 2020, Kyle Kern <kylekern0@protonmail.com>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-->

<!-- defense.xsl: Layout for the defense section -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	
	<!-- Includes: -->
	<xsl:import href="./list.xsl"/>
	<xsl:import href="./common.xsl"/>
	<xsl:import href="./dnd5e.xsl"/>
	
	<xsl:output method="html" omit-xml-declaration="yes"/>
	
	<!-- Defense - Health, armor, and resistances: -->
	<xsl:template name="Monster-Defense">
		<xsl:element name="div">
			<xsl:attribute name="id">
				<xsl:value-of select="@name"/>
				<xsl:text> Defense</xsl:text>
			</xsl:attribute>
			
			<xsl:call-template name="section-heading">
				<xsl:with-param name="text" select="'Defense'"/>
			</xsl:call-template>
			
			<!-- Armor: -->
			<xsl:call-template name="Monster-Defense-Armor"/>
			
			<!-- Hit points: -->
			<xsl:call-template name="Monster-Defense-HitPoints"/>
			
			<!-- Saving throws: -->
			<xsl:call-template name="list-of-values">
				<xsl:with-param name="container" select="SavingThrows"/>
				<xsl:with-param name="attributes" select="Attributes[last()]"/>
				<xsl:with-param name="profbonus" select="ProficiencyBonus[last()]"/>
				<xsl:with-param name="heading">Saving Throws</xsl:with-param>
				<xsl:with-param name="abbreviate">true</xsl:with-param>
			</xsl:call-template>
			
			<!-- Damage resistances, immunities, and vulnerailities: -->
			<xsl:call-template name="Monster-Defense-DamageTypes"/>
			
		</xsl:element>
	</xsl:template>
	
	<!-- Armor: -->
	<xsl:template name="Monster-Defense-Armor">
		<xsl:variable name="all_armor" select="Armors[last()]/Armor"/>
		<xsl:variable name="armor_count" select="count($all_armor)"/>
		
		<xsl:element name="p">
			
			<!-- Heading: -->
			<xsl:call-template name="feature-heading-inline">
				<xsl:with-param name="text" select="'Armor Class: '"/>
			</xsl:call-template>
			
			<!-- Total AC: -->
			<xsl:call-template name="Monster-Defense-Armor-AC">
				<xsl:with-param name="this_armor" select="$all_armor[1]"/>
				<xsl:with-param name="prev_ac" select="number(0)"/>
			</xsl:call-template>
			
			<!-- Armor listing - break down the total AC by its sources: -->
			<xsl:text> (</xsl:text>
			<xsl:choose>
				<!-- If there is only one armor, just list its description: -->
				<xsl:when test="$armor_count = 1">
					<xsl:value-of select="$all_armor[1]/."/>
				</xsl:when>
				
				<!-- Otherwise, list each of the Armors and their respective contributions: -->
				<xsl:otherwise>
					<xsl:for-each select="$all_armor">
						<xsl:call-template name="Monster-Defense-Armor-Contribution">
							<xsl:with-param name="this_armor" select="."/>
						</xsl:call-template>

						<xsl:if test="string-length(.) > 0">
							<xsl:text> </xsl:text>
							<xsl:value-of select="."/>
						</xsl:if>

						<xsl:if test="count(./following-sibling::Armor) > 0">
							<xsl:text>, </xsl:text>
						</xsl:if>
					</xsl:for-each>
				</xsl:otherwise>
			</xsl:choose>
			
			<xsl:text>)</xsl:text>
		</xsl:element>
	</xsl:template>
	
	<!-- Armor Class: -->
	<xsl:template name="Monster-Defense-Armor-AC">
		<xsl:param name="this_armor"/>  <!-- Armor to add to the total AC -->
		<xsl:param name="prev_ac"/>     <!-- Previous AC value -->
		
		<!-- Find this Armor's contribution to the AC: -->
		<xsl:variable name="this_armor_contribution">
			<xsl:call-template name="Monster-Defense-Armor-Contribution">
				<xsl:with-param name="this_armor" select="$this_armor"/>
			</xsl:call-template>
		</xsl:variable>
		
		<!-- Add this Armor's contribution to the previous AC to find the total AC up to this point: -->
		<xsl:variable name="this_ac" select="number($prev_ac + $this_armor_contribution)"/>
		
		<xsl:choose>
			<!-- Recurse into the next Armor if this is not the last: -->
			<xsl:when test="count($this_armor/following-sibling::Armor) > 0">
				<xsl:call-template name="Monster-Defense-Armor-AC">
					<xsl:with-param name="this_armor" select="($this_armor/following-sibling::Armor)[1]"/>
					<xsl:with-param name="prev_ac" select="$this_ac"/>
				</xsl:call-template>
			</xsl:when>
			<!-- Otherwise, output the total AC: -->
			<xsl:otherwise>
				<xsl:value-of select="$this_ac"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<!-- AC contribution from one armor: -->
	<xsl:template name="Monster-Defense-Armor-Contribution">
		<xsl:param name="this_armor"/>
		
		<!-- Find the flat contribution (not tied to an attribute) of this armor: -->
		<xsl:variable name="this_flat_contribution">
			<xsl:choose>
				<!-- 0 if not specified or invalid (non-numeric): -->
				<xsl:when test="not($this_armor/@value) or not(number($this_armor/@value) = $this_armor/@value)">
					<xsl:value-of select="number(0)"/>
				</xsl:when>
				<!-- Otherwise, select the value provided: -->
				<xsl:otherwise>
					<xsl:value-of select="number($this_armor/@value)"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		
		<!-- Find the attribute modifier's contribution: -->
		<xsl:variable name="this_attr_contribution">
			<xsl:choose>
				<!-- An attribute can't contribute something if it's not specified: -->
				<xsl:when test="not($this_armor/@attribute)">
					<xsl:value-of select="number(0)"/>
				</xsl:when>
				
				<xsl:otherwise>
					<xsl:variable name="this_attr_name" select="$this_armor/@attribute"/>
					
					<!-- Get the attribute modifier: -->
					<xsl:variable name="this_attr_mod">
						<xsl:call-template name="mod-from-score">
							<xsl:with-param name="score" select="(//Attribute[@name=$this_attr_name])[last()]"/>
						</xsl:call-template>
					</xsl:variable>
					
					<xsl:choose>
						<!-- We can't use this attribute if the score/mod is invalid: -->
						<xsl:when test="not(number($this_attr_mod) = $this_attr_mod)">
							<xsl:value-of select="number(0)"/>
						</xsl:when>
						
						<!-- Otherwise, make sure we don't violate the maximum contribution: -->
						<xsl:otherwise>
							<xsl:variable name="limited_attr_mod">
								<xsl:choose>
									<xsl:when test="number($this_armor/@max) = $this_armor/@max">
										<xsl:choose>
											<xsl:when test="$this_attr_mod > $this_armor/@max">
												<xsl:value-of select="$this_armor/@max"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="$this_attr_mod"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="$this_attr_mod"/>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:variable>
							<xsl:value-of select="$limited_attr_mod"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		
		<!-- The combined contribution from this piece of armor: -->
		<xsl:value-of select="number($this_flat_contribution + $this_attr_contribution)"/>
	</xsl:template>
	
	<!-- Hit Points Listing: -->
	<xsl:template name="Monster-Defense-HitPoints">
		<!-- We might need to know the CON mod: -->
		<xsl:variable name="con_mod">
			<xsl:call-template name="mod-from-score">
				<xsl:with-param name="score" select="number(((Attributes[last()])/Attribute[@name='Constitution'])[last()])"/>
			</xsl:call-template>
		</xsl:variable>
		
		<!-- Get the average of the entire hit points roll: -->
		<xsl:variable name="total_average">
			<xsl:call-template name="Monster-Defense-HitPoints-Average">
				<xsl:with-param name="hitdice" select="HitDice[last()]"/>
				<xsl:with-param name="con_mod" select="$con_mod"/>
			</xsl:call-template>
		</xsl:variable>
		
		<!-- List the hit dice: -->
		<xsl:element name="p">
			<xsl:call-template name="feature-heading-inline">
				<xsl:with-param name="text" select="'Hit Points: '"/>
			</xsl:call-template>
			
			<xsl:value-of select="$total_average"/>
			
			<xsl:text> (</xsl:text>
			<xsl:call-template name="print-dice">
				<xsl:with-param name="dice" select="HitDice[last()]"/>
				<xsl:with-param name="delim" select="' + '"/>
			</xsl:call-template>
			<!-- Manually specify the CON mod contribution: -->
			<xsl:text> + </xsl:text>
			<xsl:variable name="hitdice_count">
				<xsl:call-template name="count-dice">
					<xsl:with-param name="this_type" select="HitDice[last()]/Die[1]"/>
					<xsl:with-param name="prev_count" select="number(0)"/>
				</xsl:call-template>
			</xsl:variable>
			<xsl:variable name="con_contribution" select="number($hitdice_count * $con_mod)"/>
			<xsl:value-of select="$con_contribution"/>
			<xsl:text>)</xsl:text>
		</xsl:element>
	</xsl:template>
	
	<!-- Total average hit points: -->
	<xsl:template name="Monster-Defense-HitPoints-Average">
		<xsl:param name="hitdice"/>  <!-- Reference to a container of hit dice -->
		<xsl:param name="con_mod"/>  <!-- Constitution modifier -->
		
		<!-- Find the CON modifier's contribution (add it once per hit die): -->
		<xsl:variable name="hitdice_count">
			<xsl:call-template name="count-dice">
				<xsl:with-param name="this_type" select="$hitdice/Die[1]"/>
				<xsl:with-param name="prev_count" select="number(0)"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="con_contribution" select="number($hitdice_count * $con_mod)"/>
		
		<!-- Find the total average hit points: -->
		<xsl:variable name="total">
			<xsl:call-template name="sum-die-averages">
				<xsl:with-param name="this_die" select="$hitdice/Die[1]"/>
				<xsl:with-param name="prev_sum" select="number(0)"/>
				<xsl:with-param name="const" select="$con_contribution"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:value-of select="$total"/>
	</xsl:template>
	
	<!-- Damage Type Defenses: -->
	<xsl:template name="Monster-Defense-DamageTypes">
		<!-- Container to search within: -->
		<xsl:variable name="defenses" select="DamageDefenses[last()]"/>
		
		<!-- Resistances: -->
		<xsl:if test="$defenses/DamageDefense[@effect='Resistance']">
			<xsl:element name="p">
				<xsl:call-template name="feature-heading-inline">
					<xsl:with-param name="text" select="'Resistances: '"/>
				</xsl:call-template>
				
				<xsl:call-template name="Monster-Defense-DamageTypes-Effect">
					<xsl:with-param name="defenses" select="$defenses"/>
					<xsl:with-param name="effect" select="'Resistance'"/>
				</xsl:call-template>
			</xsl:element>
		</xsl:if>
		
		<!-- Immunities: -->
		<xsl:if test="$defenses/DamageDefense[@effect='Immunity']">
			<xsl:element name="p">
				<xsl:call-template name="feature-heading-inline">
					<xsl:with-param name="text" select="'Immunities: '"/>
				</xsl:call-template>
				
				<xsl:call-template name="Monster-Defense-DamageTypes-Effect">
					<xsl:with-param name="defenses" select="$defenses"/>
					<xsl:with-param name="effect" select="'Immunity'"/>
				</xsl:call-template>
			</xsl:element>
		</xsl:if>
		
		<!-- Vulnerabilities: -->
		<xsl:if test="$defenses/DamageDefense[@effect='Vulnerability']">
			<xsl:element name="p">
				<xsl:call-template name="feature-heading-inline">
					<xsl:with-param name="text" select="'Vulnerabilities: '"/>
				</xsl:call-template>
				
				<xsl:call-template name="Monster-Defense-DamageTypes-Effect">
					<xsl:with-param name="defenses" select="$defenses"/>
					<xsl:with-param name="effect" select="'Vulnerability'"/>
				</xsl:call-template>
			</xsl:element>
		</xsl:if>
	</xsl:template>
	
	<!-- Damage Defenses by their Effects: -->
	<xsl:template name="Monster-Defense-DamageTypes-Effect">
		<xsl:param name="defenses"/>  <!-- Container to search within -->
		<xsl:param name="effect"/>    <!-- Effect to search for -->
		
		<xsl:for-each select="$defenses/DamageDefense[@effect=$effect]">
			<xsl:value-of select="@name"/>
			
			<xsl:if test="count(following-sibling::DamageDefense[@effect=$effect]) > 0">
				<xsl:text>, </xsl:text>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	
</xsl:stylesheet>
